#!/usr/bin/octave-cli
% inputs = strsplit(fileread("sample.in"));

% tinput = input("t")

i = 1;
% t = str2num(inputs{1, i}); i += 1;
t = input("");
for c = 1:t
    nk = strsplit(input("", "s"));
    % n = str2num(inputs{1, i}); i += 1;
    n = str2num(nk{1, 1});
    % k = str2num(inputs{1, i}); i += 1;
    k = str2num(nk{1, 2});
    pancakes = zeros(n, 2);
    for p = 1:n
        rh = strsplit(input("", "s"));
        % pancakes(p, 1) = str2num(inputs{1, i}); i += 1;
        pancakes(p, 1) = str2num(rh{1, 1});
        % pancakes(p, 2) = str2num(inputs{1, i}); i += 1;
        pancakes(p, 2) = str2num(rh{1, 2});
    end
    n
    k
    % pancakes
    area = 0;
    if n == k
        % Select all
        topArea = pi * max(pancakes(:, 1)) .^ 2;
        sideAreas = 2 * pi * pancakes(:, 1) .* pancakes(:, 2);
        area = topArea + sum(sideAreas);
    else
        options = nchoosek(1:n, k);
        plates = pancakes(options', :);
        plates = reshape(plates', 2, k, size(options, 1));
        tops = plates(1, :, :);
        sides = plates(2, :, :);
        topAreas = max(tops, [], 2) .^ 2 .* pi;
        sideAreas = sum(tops .* sides, 2) .* 2 .* pi;
        areas = topAreas + sideAreas;
        area = max(areas, [], 3);
    end
    fprintf(stdout, "Case #%d: %f\n", c, area);
end

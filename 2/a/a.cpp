#include <iostream>
#include <queue>
#include <vector>

#define MAX(a, b) (a > b) ? a : b

using namespace std;

struct Pancake {
    unsigned long long radius, height;
    unsigned long long topArea, sideArea;
    double trueArea;
    void setTopArea() {
        topArea = radius * radius; // * pi
    }
    void setSideArea() {
        sideArea = 2 * radius * height; // * pi
    }
};

class topCompare {
    public:
        bool operator() (Pancake lhs, Pancake rhs) {
            return lhs.topArea < rhs.topArea;
        }
};

class sideCompare {
    public:
        bool operator() (Pancake lhs, Pancake rhs) {
            return lhs.topArea < rhs.topArea;
        }
};

unsigned long long stackEm(int k, unsigned long long topArea, vector<Pancake> pancakes) {
    if (k == 0) { return 0; }
    unsigned long long bestArea = 0;
    for (int p = 0, max = pancakes.size(); p < max; p++) {
        Pancake pancake = pancakes[p];
        vector<Pancake> remaining = pancakes;
        remaining.erase(remaining.begin() + p);
        unsigned long long newTopArea = MAX(topArea, pancake.topArea);
        unsigned long long newArea = newTopArea +
            pancake.sideArea +
            stackEm(k - 1, newTopArea, remaining);
        bestArea = MAX(bestArea, newArea);
    }
    return bestArea;
}

int main(void) {
    int t;
    cin >> t;
    cerr << t << " cases" << endl;
    for (int c = 1; c <= t; c++) {
        cerr << "case " << c << endl;
        int n, k;
        cin >> n;
        cin >> k;
        // priority_queue<Pancake, vector<Pancake>, topCompare> byTopArea;
        // priority_queue<Pancake, vector<Pancake>, sideCompare> bySideArea;
        vector<Pancake> pancakes;
        for (int p = 1; p <= n; p++) {
            Pancake pancake;
            cin >> pancake.radius;
            cin >> pancake.height;
            pancake.setTopArea();
            pancake.setSideArea();
            // byTopArea.push(pancake);
            // bySideArea.push(pancake);
            pancakes.push_back(pancake);
        }
        // cout << byTopArea.size() << endl;
        // Pancake widest = byTopArea.top();
        // cout << widest.topArea << endl;
        unsigned long long area = stackEm(k, 0, pancakes);
        cout << 3.141 * area << endl;
    }
}

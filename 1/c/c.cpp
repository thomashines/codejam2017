#include <iostream>
#include <cmath>

using namespace std;

int main(void) {
    int t;
    cin >> t;
    for (int c = 1; c <= t; c++) {
        long n, k;
        cin >> n;
        cin >> k;

        // log2(k + 1)
        long k1 = k + 1;
        long l2 = 0;
        asm("\tbsr %1, %0\n"
            : "=r" (l2)
            : "r" (k1));
        if ((long) 1 << l2 != k + 1) {
            l2++;
        }

        long divisor = (long) 1 << l2 - 1;
        long options = n - divisor + 1;
        long size;
        if (k - divisor >= (options % divisor)) {
            size = options / divisor;
        } else {
            size = (options / divisor) + 1;
        }

        long maxmin;
        long maxmax;
        if (size == 1) {
            maxmin = maxmax = 0;
        } else if (size == 2) {
            maxmin = 0;
            maxmax = 1;
        } else {
            long tosplit = size - 1;
            if (tosplit % 2) {
                // Odd
                maxmin = tosplit / 2;
                maxmax = maxmin + 1;
            } else {
                // Even
                maxmin = tosplit / 2;
                maxmax = maxmin;
            }
        }

        cout << "Case #" << c << ": " << maxmax << " " << maxmin << endl;
    }
    return 0;
}

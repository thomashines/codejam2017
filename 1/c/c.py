#!/usr/bin/env python

import fileinput
import math
import sys

if __name__ == "__main__":
    case = 0

    for line in fileinput.input():
        line = line.rstrip()

        if case == 0:
            T = int(line)
        elif case:
            # print(line, file=sys.stderr)

            split = line.split(" ")
            N = int(split[0])
            K = int(split[1])

            maxmin = 0
            maxmax = 0

            stalls = []
            unoccupied = []
            for s in range(N + 2):
                # (stall number, occupied, Ls, Rs, min, max)
                if s == 0:
                    stalls.append([0, True, 0, N, 0, N])
                elif s == N + 1:
                    stalls.append([s, True, N, 0, 0, N])
                else:
                    stalls.append([s, False, s - 1, N - s, 0, 0])
                    stalls[-1][4] = min(stalls[-1][2:4])
                    stalls[-1][5] = max(stalls[-1][2:4])
                    unoccupied.append(stalls[-1])
            # print("\t stalls %s" % stalls, file=sys.stderr)
            # print("\t stalls %s" % [1 if s[1] else 0 for s in stalls], file=sys.stderr)

            for p in range(K):
                # Sort by min(Ls, Rs) desc
                # print("\t\t stalls %s" % stalls, file=sys.stderr)
                minsorted = sorted(unoccupied, key=lambda stall: stall[4], reverse=True)

                maxmin = minsorted[0][4]
                preferred = []
                for stall in minsorted:
                    if stall[4] == maxmin:
                        preferred.append(stall)
                    else:
                        break

                if len(preferred) > 1:
                    # Sort by max(Ls, Rs) desc
                    maxsorted = sorted(preferred, key=lambda stall: stall[5], reverse=True)
                    # print("\t\t maxsorted %s" % maxsorted, file=sys.stderr)

                    maxmax = maxsorted[0][5]
                    preferred = []
                    for stall in maxsorted:
                        if stall[5] == maxmax:
                            preferred.append(stall)
                        else:
                            break

                if len(preferred) > 1:
                    # Sort by stall number asc
                    numsorted = sorted(preferred, key=lambda stall: stall[0])
                    # print("\t\t numsorted %s" % numsorted, file=sys.stderr)
                    preferred = [numsorted[0]]

                # print("\t\t preferred %s" % preferred, file=sys.stderr)

                # Occupy preferred stall
                occupy = preferred[0]
                maxmin = occupy[4]
                maxmax = occupy[5]
                # print("\t\t count %s" % unoccupied.count(occupy), file=sys.stderr)
                unoccupied.remove(occupy)
                occupy[1] = True
                occupy[2] = 0
                occupy[3] = 0

                # Adjust Ls
                s = occupy[0] + 1
                Ls = 0
                while s < N + 2 and stalls[s][2] > Ls:
                    stalls[s][2] = Ls
                    stalls[s][4] = min(stalls[s][2:4])
                    stalls[s][5] = max(stalls[s][2:4])
                    s += 1
                    Ls += 1

                # Adjust Rs
                s = occupy[0] - 1
                Rs = 0
                while s >= 0 and stalls[s][3] > Rs:
                    stalls[s][3] = Rs
                    stalls[s][4] = min(stalls[s][2:4])
                    stalls[s][5] = max(stalls[s][2:4])
                    s -= 1
                    Rs += 1

                # print("\t\t stalls %s" % [1 if s[1] else 0 for s in stalls], file=sys.stderr)

            # print("Case #%d: %d %d" % (case, maxmax, maxmin), file=sys.stderr)
            print("Case #%d: %d %d" % (case, maxmax, maxmin))

        case += 1

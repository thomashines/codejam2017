#include <iostream>
#include <string>
#include <vector>

#include <glpk.h>

using namespace std;

#define MIN(a, b) (a <= b) ? a : b
#define MAX(a, b) (a >= b) ? a : b

class Model {
    public:
        char type;
        int row;
        int column;
        Model(char type, int row, int column): type(type), row(row),
            column(column) { }
        Model(int row, int column): row(row), column(column) { }
        bool operator==(Model &model) {
            return model.type == type && model.row == row &&
                model.column == column;
        }
        bool operator!=(Model &model) {
            return model.type != type || model.row != row ||
                model.column != column;
        }
};

ostream& operator<<(ostream &stream, const Model &model) {
    return stream << "<" << model.type << "@(" << model.row << "," << model.column << ")>";
}

int ci(int n, int row, int column, char type) {
    // cells are size 4
    // rows are size 4*n
    int t = 0;
    switch (type) {
        case '.': t = 0; break;
        case '+': t = 1; break;
        case 'x': t = 2; break;
        case 'o': t = 3; break;
    }
    // cerr << n << endl;
    // cerr << row << endl;
    // cerr << column << endl;
    // cerr << type << endl;
    return row * 4 * n + column * 4 + t + 1;
}

int main(void) {
    int cases;
    cin >> cases;
    // cerr << "cases " << cases << endl;

    for (int c = 0; c < cases; c++) {
    // for (int c = 0; c < 1; c++) {
        cerr << "Case #" << c + 1 << endl;
        int n, models;
        cin >> n;
        cin >> models;
        // cerr << "n " << n << endl;

        vector<Model> modelsVector;

        for (int model = 0; model < models; model++) {
            char type;
            cin >> type;
            int row, column;
            cin >> row;
            cin >> column;
            // cerr << row << " " << column << " " << type << endl;
            modelsVector.push_back(Model(type, row - 1, column - 1));
            // cerr << modelsVector[model] << endl;
        }

        // Create problem
        glp_prob *lp;
        lp = glp_create_prob();
        glp_term_out(0);
        glp_set_prob_name(lp, "fashion");

        // Create variables

        // Add rows
        int variables = n * n * 4;
        glp_add_cols(lp, variables);

        // Set names and types
        char *name = (char *) malloc(10);
        // for (int v = 1; v <= variables; v++) {
        //     sprintf(name, "x_%d", v);
        //     glp_set_col_name(lp, v, name);
        //     glp_set_col_kind(lp, v, GLP_BV);
        // }
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                sprintf(name, "x_%d_%d_%c", row, column, 'd');
                glp_set_col_name(lp, ci(n, row, column, '.'), name);
                glp_set_col_kind(lp, ci(n, row, column, '.'), GLP_BV);
                // glp_set_col_kind(lp, ci(n, row, column, '.'), GLP_IV);
                // glp_set_col_bnds(lp, ci(n, row, column, '.'), GLP_DB, 0, 1);
                sprintf(name, "x_%d_%d_%c", row, column, 'p');
                glp_set_col_name(lp, ci(n, row, column, '+'), name);
                glp_set_col_kind(lp, ci(n, row, column, '+'), GLP_BV);
                // glp_set_col_kind(lp, ci(n, row, column, '+'), GLP_IV);
                // glp_set_col_bnds(lp, ci(n, row, column, '+'), GLP_DB, 0, 1);
                sprintf(name, "x_%d_%d_%c", row, column, 'x');
                glp_set_col_name(lp, ci(n, row, column, 'x'), name);
                glp_set_col_kind(lp, ci(n, row, column, 'x'), GLP_BV);
                // glp_set_col_kind(lp, ci(n, row, column, 'x'), GLP_IV);
                // glp_set_col_bnds(lp, ci(n, row, column, 'x'), GLP_DB, 0, 1);
                sprintf(name, "x_%d_%d_%c", row, column, 'o');
                glp_set_col_name(lp, ci(n, row, column, 'o'), name);
                glp_set_col_kind(lp, ci(n, row, column, 'o'), GLP_BV);
                // glp_set_col_kind(lp, ci(n, row, column, 'o'), GLP_IV);
                // glp_set_col_bnds(lp, ci(n, row, column, 'o'), GLP_DB, 0, 1);
            }
        }

        // Create constraints

        // modelsVector.size() initial constraints
        int initials = modelsVector.size();
        // n^2 cell constraints
        int cells = n * n;
        // n row constraints
        int rows = n;
        // n column constraints
        int columns = n;
        // 4n-6 diagonal constraints
        int diagonals = MAX(0, 4 * n - 6);
        // Total
        int constraints = diagonals + rows + columns + cells + initials;

        // cerr << "initials " << initials << endl;
        // cerr << "cells " << cells << endl;
        // cerr << "rows " << rows << endl;
        // cerr << "columns " << columns << endl;
        // cerr << "diagonals " << diagonals << endl;
        // cerr << "constraints " << constraints << endl;
        // cerr << "allocating " << (constraints * variables + 1) << endl;

        // Add rows
        glp_add_rows(lp, constraints);

        // Create matrix
        int *ia = (int *)
            malloc(sizeof(int) * (constraints * variables + 1));
        int *ja = (int *)
            malloc(sizeof(int) * (constraints * variables + 1));
        double *ar = (double *)
            malloc(sizeof(double) * (constraints * variables + 1));

        // Set constraints
        int r = 1;
        int a = 1;

        // Initials
        for (int i = 0; i < initials; i++) {
            // cerr << modelsVector[i] << endl;
            ia[a] = r + i;
            ja[a] = ci(n, modelsVector[i].row, modelsVector[i].column,
                modelsVector[i].type);
            ar[a++] = 1;
            if (modelsVector[i].type == '+' || modelsVector[i].type == 'x') {
                ia[a] = r + i;
                ja[a] = ci(n, modelsVector[i].row, modelsVector[i].column,
                    'o');
                ar[a++] = 1;
            }
            glp_set_row_bnds(lp, r + i, GLP_FX, 1.0, 1.0);
        }
        r += initials;

        // Cells
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                ia[a] = r + row * n + column;
                ja[a] = ci(n, row, column, '.');
                ar[a++] = 1;
                ia[a] = r + row * n + column;
                ja[a] = ci(n, row, column, '+');
                ar[a++] = 1;
                ia[a] = r + row * n + column;
                ja[a] = ci(n, row, column, 'x');
                ar[a++] = 1;
                ia[a] = r + row * n + column;
                ja[a] = ci(n, row, column, 'o');
                ar[a++] = 1;
                glp_set_row_bnds(lp, r + row * n + column, GLP_FX, 1.0, 1.0);
            }
        }
        r += cells;

        // Rows
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                ia[a] = r + row;
                ja[a] = ci(n, row, column, 'x');
                ar[a++] = 1;
                ia[a] = r + row;
                ja[a] = ci(n, row, column, 'o');
                ar[a++] = 1;
            }
            glp_set_row_bnds(lp, r + row, GLP_UP, 0.0, 1.0);
        }
        r += rows;

        // Columns
        for (int column = 0; column < columns; column++) {
            for (int row = 0; row < rows; row++) {
                ia[a] = r + column;
                ja[a] = ci(n, row, column, 'x');
                ar[a++] = 1;
                ia[a] = r + column;
                ja[a] = ci(n, row, column, 'o');
                ar[a++] = 1;
            }
            glp_set_row_bnds(lp, r + column, GLP_UP, 0.0, 1.0);
        }
        r += columns;

        // Make diagonals
        vector<vector<Model>> diagonalsVector(diagonals);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                int forward = row + column - 1;
                if (forward >= 0 && forward < diagonals / 2) {
                    diagonalsVector[forward].push_back(Model(row, column));
                }
                int backward = row - column + n - 2;
                if (backward >= 0 && backward < diagonals / 2) {
                    diagonalsVector[diagonals / 2 + backward].push_back(
                        Model(row, column));
                }
            }
        }
        for (int d = 0; d < diagonals; d++) {
            for (unsigned int e = 0; e < diagonalsVector[d].size(); e++) {
                // cerr << diagonalsVector[d][e] << ", ";
                ia[a] = r + d;
                ja[a] = ci(n, diagonalsVector[d][e].row,
                    diagonalsVector[d][e].column, '+');
                ar[a++] = 1;
                ia[a] = r + d;
                ja[a] = ci(n, diagonalsVector[d][e].row,
                    diagonalsVector[d][e].column, 'o');
                ar[a++] = 1;
            }
            // cerr << endl;
            glp_set_row_bnds(lp, r + d, GLP_UP, 0.0, 1.0);
        }
        r += diagonals;

        // Create objective
        glp_set_obj_dir(lp, GLP_MAX);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                glp_set_obj_coef(lp, ci(n, row, column, '.'), 0);
                glp_set_obj_coef(lp, ci(n, row, column, '+'), 1);
                glp_set_obj_coef(lp, ci(n, row, column, 'x'), 1);
                glp_set_obj_coef(lp, ci(n, row, column, 'o'), 2);
            }
        }

        // Load matrix
        // for (int aa = 1; aa < a; aa++) {
        //     cerr << ia[aa] << " " << ja[aa] << " " << ar[aa] << endl;
        // }
        // cerr << "matrix cells used = " << a << "/" <<
        //     (constraints * constraints + 1) << endl;
        glp_load_matrix(lp, a - 1, ia, ja, ar);

        // Write to LP file
        // sprintf(name, "lp/d%03d.lp", c);
        // glp_write_lp(lp, NULL, name);

        // Solve
        cerr << "simplex" << endl;
        // glp_simplex(lp, NULL);
        cerr << "intopt" << endl;
        glp_iocp parm;
        glp_init_iocp(&parm);
        parm.presolve = GLP_ON;
        parm.binarize = GLP_ON;
        glp_intopt(lp, &parm);
        cerr << "done" << endl;

        // Get solution
        int score = glp_get_obj_val(lp);
        // cerr << "score " << score << endl;

        // Get changes
        vector<Model> changes;
        string stage = "";
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                // cerr << glp_mip_col_val(lp, ci(n, row, column, '.')) << endl;
                // cerr << glp_mip_col_val(lp, ci(n, row, column, '+')) << endl;
                // cerr << glp_mip_col_val(lp, ci(n, row, column, 'x')) << endl;
                // cerr << glp_mip_col_val(lp, ci(n, row, column, 'o')) << endl;
                bool d = glp_mip_col_val(lp, ci(n, row, column, '.')) > 0.5;
                bool p = glp_mip_col_val(lp, ci(n, row, column, '+')) > 0.5;
                bool x = glp_mip_col_val(lp, ci(n, row, column, 'x')) > 0.5;
                bool o = glp_mip_col_val(lp, ci(n, row, column, 'o')) > 0.5;
                char type;
                if (d) { type = '.'; }
                else if (p) { type = '+'; }
                else if (x) { type = 'x'; }
                else if (o) { type = 'o'; }
                else { type = '-'; }
                stage += type;

                // Check if changed
                if (type != '.' && type != '-') {
                    Model model = Model(type, row, column);
                    bool changed = true;
                    for (unsigned int m = 0; m < modelsVector.size(); m++) {
                        if (model == modelsVector[m]) {
                            changed = false;
                            break;
                        }
                    }
                    if (changed) {
                        changes.push_back(model);
                    }
                }
            }
            stage += "\n";
        }
        // cerr << stage;

        cout << "Case #" << c << ": " << score << " " << changes.size() << endl;

        for (unsigned int c = 0; c < changes.size(); c++) {
            // cerr << changes[c] << endl;
            cout << changes[c].type << " " << changes[c].row + 1 << " " <<
                changes[c].column + 1 << endl;
        }

        // Clean up
        free(name);
        free(ia);
        free(ja);
        free(ar);
        glp_delete_prob(lp);
    }

    glp_free_env();
    return 0;
}

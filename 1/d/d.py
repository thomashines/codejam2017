#!/usr/bin/env python

from pulp import *
import fileinput
import math
import re
import sys
import time

def get_forwards(N):
    forwards = [[] for f in range(2 * N - 1)]
    for r in range(N):
        for c in range(N):
            forwards[r + c].append((r, c))
    return [f for f in forwards if len(f) > 1]

def get_backwards(N):
    backwards = [[] for f in range(2 * N - 1)]
    for r in range(N):
        for c in range(N):
            backwards[r - c + N - 1].append((r, c))
    return [b for b in backwards if len(b) > 1]

if __name__ == "__main__":
    starttime = time.time()

    T = 0
    case = 0
    cases = []
    newcase = []
    M = -1

    for line in fileinput.input():
        line = line.rstrip()

        if case == 0:
            T = int(line)
            case += 1
        else:
            # Get specification
            spec = re.search("^(\d+) (\d+)$", line)
            if spec is not None:
                N = int(spec.group(1))
                M = int(spec.group(2))
                newcase = [N, []]
            elif M > 0:
                # Get model
                model = re.search("^([\+xo]) (\d+) (\d+)$", line)
                if model is not None:
                    type = model.group(1)
                    row = int(model.group(2)) - 1
                    column = int(model.group(3)) - 1
                    newcase[1].append((row, column, type))
                    M -= 1
        if M == 0:
            cases.append(newcase)
        if len(cases) == T:
            break

    setuptime = 0
    runtime = 0

    casenum = 1
    for case in cases[:3]:
        N = case[0]

        rows = list(range(N))
        columns = rows
        options = [".", "+", "x", "o"]

        setupstart = time.time()

        prob = LpProblem("", LpMaximize)

        choices = LpVariable.dicts("", (rows, columns, options), 0, 1, LpInteger)
        choices_flat_cells = [
            choice for row in choices.values() for choice in row.values()]
        # print(choices_flat_cells, file=sys.stderr)

        prob += lpSum(cell['+'] + cell['x'] + 2 * cell['o'] for cell in choices_flat_cells), ""

        # One type per cell constraint
        for cell in choices_flat_cells:
            prob += lpSum(choice for choice in cell.values()) == 1, ""

        # Rows and columns cannot have more than one x or o
        for row in choices.values():
            prob += lpSum(column['x'] + column['o'] for column in row.values()) <= 1, ""

        for column in columns:
            prob += lpSum(row[column]['x'] + row[column]['o'] for row in choices.values()) <= 1, ""

        # Diagonals cannot have more than one + or o
        forwards = get_forwards(N)
        backwards = get_backwards(N)
        for diagonal in forwards + backwards:
            # print(diagonal, file=sys.stderr)
            prob += lpSum(choices[d[0]][d[1]]['+'] + choices[d[0]][d[1]]['o'] for d in diagonal) <= 1, ""

        # Set initial conditions
        for initial in case[1]:
            if initial[2] in "+x":
                prob += choices[initial[0]][initial[1]][initial[2]] + choices[initial[0]][initial[1]]['o'] == 1, ""
            else:
                prob += choices[initial[0]][initial[1]][initial[2]] == 1, ""

        setuptime += time.time() - setupstart

        # prob.writeLP("case%03d.lp" % casenum)

        runstart = time.time()

        # Solve the problem
        prob.solve(solver=solvers.COIN())
        # print("Status:", LpStatus[prob.status], file=sys.stderr)
        # print("Modeness:", value(prob.objective), file=sys.stderr)

        runtime += time.time() - runstart

        # Make a string from the variables
        models = []
        string = ""
        for r in rows:
            for c in columns:
                for t in options:
                    if value(choices[r][c][t]) == 1:
                        string += t
                        if t != '.':
                            model = (r, c, t)
                            if model not in case[1]:
                                models.append(model)
            string += "\n"
        # print("#%d" % casenum, file=sys.stderr)
        # print(string, file=sys.stderr)
        # print(string)
        # print(case[1], file=sys.stderr)
        # print(models, file=sys.stderr)

        # Write output
        print("Case #%d: %d %d" % (casenum, int(value(prob.objective)), len(models)))
        for model in models:
            print("%s %d %d" % (model[2], model[0] + 1, model[1] + 1))

        print("setuptime %fs" % setuptime, file=sys.stderr)
        # print("setuptime %fs" % setuptime)
        print("runtime %fs" % runtime, file=sys.stderr)
        # print("runtime %fs" % runtime)
        print("totaltime %fs" % (time.time() - starttime), file=sys.stderr)
        # print("totaltime %fs" % (time.time() - starttime))

        # Next case
        casenum += 1

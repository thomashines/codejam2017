#include <iostream>
#include <string>
#include <vector>

#include <coin/CbcModel.hpp>
#include <coin/CbcSolver.hpp>
#include <coin/OsiCbcSolverInterface.hpp>
#include <coin/OsiClpSolverInterface.hpp>

using namespace std;

#define MIN(a, b) (a <= b) ? a : b
#define MAX(a, b) (a >= b) ? a : b

class Model {
    public:
        char type;
        int row;
        int column;
        Model(char type, int row, int column): type(type), row(row),
            column(column) { }
        Model(int row, int column): row(row), column(column) { }
        bool operator==(Model &model) {
            return model.type == type && model.row == row &&
                model.column == column;
        }
        bool operator!=(Model &model) {
            return model.type != type || model.row != row ||
                model.column != column;
        }
};

ostream& operator<<(ostream &stream, const Model &model) {
    return stream << "<" << model.type << "@(" << model.row << "," << model.column << ")>";
}

int ci(int n, int row, int column, char type) {
    // cells are size 4
    // rows are size 4*n
    int t = 0;
    switch (type) {
        case '.': t = 0; break;
        case '+': t = 1; break;
        case 'x': t = 2; break;
        case 'o': t = 3; break;
    }
    // cerr << n << endl;
    // cerr << row << endl;
    // cerr << column << endl;
    // cerr << type << endl;
    return row * 4 * n + column * 4 + t;
}

int main(void) {
    int cases;
    cin >> cases;
    // cerr << "cases " << cases << endl;

    for (int c = 0; c < cases; c++) {
    // for (int c = 0; c < 3; c++) {
        cerr << "Case #" << c + 1 << endl;
        int n, models;
        cin >> n;
        cin >> models;
        // cerr << "n " << n << endl;

        vector<Model> modelsVector;

        for (int model = 0; model < models; model++) {
            char type;
            cin >> type;
            int row, column;
            cin >> row;
            cin >> column;
            // cerr << row << " " << column << " " << type << endl;
            modelsVector.push_back(Model(type, row - 1, column - 1));
            // cerr << modelsVector[model] << endl;
        }

        // Create problem
        OsiCbcSolverInterface *si = new OsiCbcSolverInterface();
        si->setObjSense(-1);
        CoinMessageHandler messageHandler = CoinMessageHandler();
        messageHandler.setFilePointer(stderr);
        // messageHandler.setLogLevel(4);
        messageHandler.setLogLevel(0);
        si->passInMessageHandler(&messageHandler);

        // Create variables

        // Add rows
        int variables = n * n * 4;
        double *objective = new double[variables];
        double *col_lb = new double[variables];
        double *col_ub = new double[variables];

        // Set names and types
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                objective[ci(n, row, column, '.')] = 0;
                objective[ci(n, row, column, '+')] = 1;
                objective[ci(n, row, column, 'x')] = 1;
                objective[ci(n, row, column, 'o')] = 2;

                col_lb[ci(n, row, column, '.')] = 0;
                col_lb[ci(n, row, column, '+')] = 0;
                col_lb[ci(n, row, column, 'x')] = 0;
                col_lb[ci(n, row, column, 'o')] = 0;

                col_ub[ci(n, row, column, '.')] = 1;
                col_ub[ci(n, row, column, '+')] = 1;
                col_ub[ci(n, row, column, 'x')] = 1;
                col_ub[ci(n, row, column, 'o')] = 1;
            }
        }

        // Create constraints

        // modelsVector.size() initial constraints
        int initials = modelsVector.size();
        // n^2 cell constraints
        int cells = n * n;
        // n row constraints
        int rows = n;
        // n column constraints
        int columns = n;
        // 4n-6 diagonal constraints
        int diagonals = MAX(0, 4 * n - 6);
        // Total
        int constraints = diagonals + rows + columns + cells + initials;

        // cerr << "initials " << initials << endl;
        // cerr << "cells " << cells << endl;
        // cerr << "rows " << rows << endl;
        // cerr << "columns " << columns << endl;
        // cerr << "diagonals " << diagonals << endl;
        // cerr << "constraints " << constraints << endl;
        // cerr << "allocating " << (constraints * variables + 1) << endl;

        // Create matrix
        double *row_lb = (double *) malloc(sizeof(double) * (constraints));
        double *row_ub = (double *) malloc(sizeof(double) * (constraints));
        CoinPackedMatrix *constraintMatrix = new CoinPackedMatrix(false, 0, 0);
        constraintMatrix->setDimensions(0, variables);

        // Set constraints
        int r = 0;

        // Initials
        for (int i = 0; i < initials; i++) {
            // cerr << modelsVector[i] << endl;
            CoinPackedVector constraint;
            constraint.insert(ci(n, modelsVector[i].row, modelsVector[i].column,
                modelsVector[i].type), 1);
            if (modelsVector[i].type == '+' || modelsVector[i].type == 'x') {
                constraint.insert(ci(n, modelsVector[i].row,
                    modelsVector[i].column, 'o'), 1);
            }
            row_lb[r] = 1;
            row_ub[r++] = 1;
            constraintMatrix->appendRow(constraint);
        }

        // Cells
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                // cout << "cell constraint " << r << endl;
                CoinPackedVector constraint;
                constraint.insert(ci(n, row, column, '.'), 1);
                constraint.insert(ci(n, row, column, '+'), 1);
                constraint.insert(ci(n, row, column, 'x'), 1);
                constraint.insert(ci(n, row, column, 'o'), 1);
                row_lb[r] = 1;
                row_ub[r++] = 1;
                constraintMatrix->appendRow(constraint);
            }
        }

        // Rows
        for (int row = 0; row < rows; row++) {
            CoinPackedVector constraint;
            for (int column = 0; column < columns; column++) {
                constraint.insert(ci(n, row, column, 'x'), 1);
                constraint.insert(ci(n, row, column, 'o'), 1);
            }
            row_lb[r] = 0;
            row_ub[r++] = 1;
            constraintMatrix->appendRow(constraint);
        }

        // Columns
        for (int column = 0; column < columns; column++) {
            CoinPackedVector constraint;
            for (int row = 0; row < rows; row++) {
                constraint.insert(ci(n, row, column, 'x'), 1);
                constraint.insert(ci(n, row, column, 'o'), 1);
            }
            row_lb[r] = 0;
            row_ub[r++] = 1;
            constraintMatrix->appendRow(constraint);
        }

        // Make diagonals
        vector<vector<Model>> diagonalsVector(diagonals);
        if (diagonals > 0) {
            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {
                    int forward = row + column - 1;
                    if (forward >= 0 && forward < diagonals / 2) {
                        diagonalsVector[forward].push_back(Model(row, column));
                    }
                    int backward = row - column + n - 2;
                    if (backward >= 0 && backward < diagonals / 2) {
                        diagonalsVector[diagonals / 2 + backward].push_back(
                            Model(row, column));
                    }
                }
            }
        }
        for (int d = 0; d < diagonals; d++) {
            CoinPackedVector constraint;
            for (unsigned int e = 0; e < diagonalsVector[d].size(); e++) {
                // cerr << diagonalsVector[d][e] << ", ";
                constraint.insert(ci(n, diagonalsVector[d][e].row,
                    diagonalsVector[d][e].column, '+'), 1);
                constraint.insert(ci(n, diagonalsVector[d][e].row,
                    diagonalsVector[d][e].column, 'o'), 1);
            }
            // cerr << endl;
            row_lb[r] = 0;
            row_ub[r++] = 1;
            constraintMatrix->appendRow(constraint);
        }

        // Load matrix
        si->loadProblem(*constraintMatrix, col_lb, col_ub, objective,
            row_lb, row_ub);

        // Make integer and set name
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                si->setInteger(ci(n, row, column, '.'));
                si->setInteger(ci(n, row, column, '+'));
                si->setInteger(ci(n, row, column, 'x'));
                si->setInteger(ci(n, row, column, 'o'));
                si->setColName(ci(n, row, column, '.'), "x_" + to_string(row) +
                    "_" + to_string(column) + "_d");
                si->setColName(ci(n, row, column, '+'), "x_" + to_string(row) +
                    "_" + to_string(column) + "_p");
                si->setColName(ci(n, row, column, 'x'), "x_" + to_string(row) +
                    "_" + to_string(column) + "_x");
                si->setColName(ci(n, row, column, 'o'), "x_" + to_string(row) +
                    "_" + to_string(column) + "_o");
            }
        }

        // Write to file
        char name[10];
        sprintf(name, "lp/d2%03d", c + 1);
        si->writeLp(name);

        // Solve
        CbcModel *cbcmodel = new CbcModel(*si);
        cbcmodel->passInMessageHandler(&messageHandler);
        cbcmodel->initialSolve();
        // cbcmodel->branchAndBound();

        // Get objective
        double score = cbcmodel->getBestPossibleObjValue();
        // double score = 1;

        // Get changes
        // const double *solution = cbcmodel->bestSolution();
        // const double *solution = cbcmodel->currentSolution();
        const double *solution = cbcmodel->getColSolution();
        // const double *solution = new double[variables];
        vector<Model> changes;
        string stage = "";
        for (int row = 0; row < n; row++) {
            for (int column = 0; column < n; column++) {
                // cerr << solution[ci(n, row, column, '.')] << endl;
                // cerr << solution[ci(n, row, column, '+')] << endl;
                // cerr << solution[ci(n, row, column, 'x')] << endl;
                // cerr << solution[ci(n, row, column, 'o')] << endl;
                bool d = solution[ci(n, row, column, '.')] > 0.5;
                bool p = solution[ci(n, row, column, '+')] > 0.5;
                bool x = solution[ci(n, row, column, 'x')] > 0.5;
                bool o = solution[ci(n, row, column, 'o')] > 0.5;
                char type;
                if (d) { type = '.'; }
                else if (p) { type = '+'; }
                else if (x) { type = 'x'; }
                else if (o) { type = 'o'; }
                else { type = '-'; }
                stage += type;

                // Check if changed
                if (type != '.' && type != '-') {
                    Model model = Model(type, row, column);
                    bool changed = true;
                    for (unsigned int m = 0; m < modelsVector.size(); m++) {
                        if (model == modelsVector[m]) {
                            changed = false;
                            break;
                        }
                    }
                    if (changed) {
                        changes.push_back(model);
                    }
                }
            }
            stage += "\n";
        }
        cerr << stage;
        // cout << stage;

        cout << "Case #" << (c + 1) << ": " << score << " " << changes.size() << endl;
        for (unsigned int c = 0; c < changes.size(); c++) {
            // cerr << changes[c] << endl;
            cout << changes[c].type << " " << changes[c].row + 1 << " " <<
                changes[c].column + 1 << endl;
        }

        // Clean up
    }

    // glp_free_env();
    return 0;
}

#!/usr/bin/env python

def get_forwards(N):
    forwards = [[] for f in range(2 * N - 1)]
    for r in range(N):
        for c in range(N):
            forwards[r + c].append((r, c))
    return [f for f in forwards if len(f) > 1]

def get_backwards(N):
    backwards = [[] for f in range(2 * N - 1)]
    for r in range(N):
        for c in range(N):
            backwards[r - c + N - 1].append((r, c))
    return [b for b in backwards if len(b) > 1]

N = 4

print(get_forwards(N))
print(get_backwards(N))

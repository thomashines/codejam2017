#include <iostream>
#include <cmath>

using namespace std;

int main(void) {
    int t;
    cin >> t;
    for (int c = 1; c <= t; c++) {
        unsigned long n;
        cin >> n;
        char s[20];
        sprintf(s, "%lu", n);
        unsigned long l = floor(log10(n)) + 1;
        for (unsigned long d = l - 1; d > 0; d--) {
            if (s[d - 1] > s[d]) {
                n = n / ((unsigned long) pow(10, l - d)) *
                    ((unsigned long) pow(10,  l - d)) - 1;
                sprintf(s, "%lu", n);
            }
        }
        cout << "Case #" << c << ": " << s << endl;
    }
    return 0;
}
